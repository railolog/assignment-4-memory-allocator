#include "test.h"
#include "stdio.h"


int main(){
    int errors = 0;

    for (size_t i = 0; i < tests_count; i++){
        errors += !tests[i]();
    }

    if (!errors){
        printf("Tests passed successfully!\n");
    } else {
        fprintf(stderr, "%d test(s) did not pass.\n", errors);
    }

    return 0;
}