#ifndef MEMORY_ALLOCATOR_TEST_H

#include "stdbool.h"
#include "stddef.h"

typedef bool (test) (void);

extern test* tests[];
extern size_t tests_count;

enum {
    HEAP_SIZE = 10000,
    DEFAULT_BLOCK_SIZE = 1337,
    MAX_BLOCK_SIZE = 228228
};

#define MEMORY_ALLOCATOR_TEST_H

#endif //MEMORY_ALLOCATOR_TEST_H
