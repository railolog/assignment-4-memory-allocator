#define _DEFAULT_SOURCE

#include "test.h"
#include "mem.h"
#include "mem_internals.h"

static struct block_header* block_get_header(void* contents) {
    return (struct block_header*) (((uint8_t*)contents)-offsetof(struct block_header, contents));
}

static void heap_free(void* heap) {
    munmap(heap, size_from_capacity((block_capacity){.bytes = HEAP_SIZE}).bytes);
}

static void print_msg(char* msg){
    printf("~~~ %s ~~~\n\n", msg);
}

static void print_err(char* msg){
    printf("ERROR: %s", msg);
}

static void print_test_passed(int n){
    printf("Test %d passed successfully\n", n);
    printf("-------------------------------\n");
}

bool test_simple(){
    printf("\n\n*** Test 1: simple malloc ***\n");

    void* heap = heap_init(HEAP_SIZE);

    void* ptr = _malloc(DEFAULT_BLOCK_SIZE);

    debug_heap(stdout, heap);

    if (!ptr || block_get_header(ptr)->capacity.bytes != DEFAULT_BLOCK_SIZE){
        print_err("Failed to allocate memory");
        heap_free(heap);
        return false;
    }

    _free(ptr);

    heap_free(heap);

    print_test_passed(1);
    return true;
}

bool test_free_one_block(){
    printf("\n\n*** Test 2: free() to one of several blocks ***\n");

    void* heap = heap_init(HEAP_SIZE);

    void* b1 = _malloc(DEFAULT_BLOCK_SIZE);
    void* b2 = _malloc(DEFAULT_BLOCK_SIZE);
    void* b3 = _malloc(DEFAULT_BLOCK_SIZE);

    if (!b1 || !b2 || !b3){
        print_err("Failed to allocate memory");
        heap_free(heap);
        return false;
    }

    debug_heap(stdout, heap);
    print_msg("Three blocks allocated");

    _free(b2);
    debug_heap(stdout, heap);

    if (!block_get_header(b2)->is_free){
        print_err("Failed to free the 2nd block");
        heap_free(heap);
        return false;
    }

    print_msg("The 2nd block was freed");

    heap_free(heap);

    print_test_passed(2);
    return true;
}

bool test_free_two_blocks(){
    printf("\n\n*** Test 3: free() to two of several blocks ***\n");

    void* heap = heap_init(HEAP_SIZE);

    void* b1 = _malloc(DEFAULT_BLOCK_SIZE);
    void* b2 = _malloc(DEFAULT_BLOCK_SIZE);
    void* b3 = _malloc(DEFAULT_BLOCK_SIZE);
    void* b4 = _malloc(DEFAULT_BLOCK_SIZE);

    if (!b1 || !b2 || !b3 || !b4){
        print_err("Failed to allocate memory");
        heap_free(heap);
        return false;
    }

    debug_heap(stdout, heap);
    print_msg("Four blocks allocated");

    _free(b3);
    _free(b2);

    debug_heap(stdout, heap);

    if (!block_get_header(b2)->is_free || !block_get_header(b3)->is_free){
        print_err("Failed to free the 2nd or the 3th block");
        heap_free(heap);
        return false;
    }

    print_msg("The 2nd and th 3th blocks were freed");

    heap_free(heap);

    print_test_passed(3);
    return true;
}

bool test_extend_old_region(){
    printf("\n\n*** Test 4: extend old region ***\n");

    void* heap = heap_init(HEAP_SIZE);

    void* b1 = _malloc(DEFAULT_BLOCK_SIZE);
    void* b2 = _malloc(MAX_BLOCK_SIZE);


    debug_heap(stdout, heap);

    if (!b1 || !b2){
        print_err("Failed to allocate memory");
        heap_free(heap);
        return false;
    }

    struct block_header* b1_header = block_get_header(b1);
    struct block_header* b2_header = block_get_header(b2);

    if (b1_header->next != b2_header || b2_header->capacity.bytes != MAX_BLOCK_SIZE){
        print_err("Failed to extend old region");
        heap_free(heap);
        return false;
    }

    print_msg("Old region was extended");

    heap_free(heap);

    print_test_passed(4);
    return true;
}

bool test_not_continuous_region(){
    printf("\n\n*** Test 5: extend not continuous region ***\n");

    void* heap = heap_init(HEAP_SIZE);

    void* b1 = _malloc(HEAP_SIZE);
    struct block_header* b1_header = block_get_header(b1);

    if (!b1){
        print_err("Failed to allocate the 1st block");
        heap_free(heap);
        return false;
    }

    int a = MAP_PRIVATE;
    void* after_heap = (void *) b1_header->contents + b1_header->capacity.bytes;
    void* garbage = mmap(after_heap,
                         REGION_MIN_SIZE,
                         PROT_READ | PROT_WRITE,
                         MAP_PRIVATE | MAP_FIXED_NOREPLACE | MAP_ANONYMOUS,
                         -1,
                         0);

    void* b2 = _malloc(DEFAULT_BLOCK_SIZE);

    debug_heap(stdout, heap);

    if (!b2){
        print_err("Failed to allocate the 2nd block");
        heap_free(heap);
        return false;
    }
    if (block_get_header(b1)->next != block_get_header(b2)){
        print_err("Failed to bind blocks from not continuous regions");
        heap_free(heap);
        return false;
    }

    print_msg("Extended not continuous region");

    heap_free(heap);
    munmap(garbage, REGION_MIN_SIZE);

    print_test_passed(5);
    return true;
}

size_t tests_count = 5;
test* tests[] = {
        test_simple,
        test_free_one_block,
        test_free_two_blocks,
        test_extend_old_region,
        test_not_continuous_region
};